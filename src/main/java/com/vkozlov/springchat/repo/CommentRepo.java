package com.vkozlov.springchat.repo;

import com.vkozlov.springchat.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepo extends JpaRepository<Comment, Long> {
}
