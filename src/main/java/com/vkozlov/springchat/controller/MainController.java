package com.vkozlov.springchat.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.vkozlov.springchat.domain.User;
import com.vkozlov.springchat.domain.Views;
import com.vkozlov.springchat.repo.MessageRepo;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {
    private final MessageRepo messageRepo;
    private final ObjectWriter writer;

    @Autowired
    public MainController(MessageRepo messageRepo, ObjectMapper mapper) {
        this.messageRepo = messageRepo;

        this.writer = mapper
            .setConfig(mapper.getSerializationConfig())
            .writerWithView(Views.FullMessage.class);
    }

    @GetMapping
    public String main(Model model, @AuthenticationPrincipal User user) throws JsonProcessingException {
        Map<Object, Object> data = new HashMap<>();

        String messages = null;
        if (user != null) {
            data.put("profile", user);
            messages = writer.writeValueAsString(messageRepo.findAll());
        }

        model.addAttribute("frontendData", data);
        model.addAttribute("messages", messages != null ? messages : "[]");
        model.addAttribute("isDevMode", true);

        return "index";
    }
}
