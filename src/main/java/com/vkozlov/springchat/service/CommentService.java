package com.vkozlov.springchat.service;

import com.vkozlov.springchat.domain.Comment;
import com.vkozlov.springchat.domain.User;
import com.vkozlov.springchat.domain.Views;
import com.vkozlov.springchat.dto.EventType;
import com.vkozlov.springchat.dto.ObjectType;
import com.vkozlov.springchat.repo.CommentRepo;
import com.vkozlov.springchat.util.WsSender;
import java.util.function.BiConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    private final CommentRepo commentRepo;
    private final BiConsumer<EventType, Comment> wsSender;

    @Autowired
    public CommentService(CommentRepo commentRepo, WsSender wsSender) {
        this.commentRepo = commentRepo;
        this.wsSender = wsSender.getSender(ObjectType.COMMENT, Views.FullComment.class);
    }

    public Comment create (Comment comment, User user) {
        comment.setAuthor(user);
        Comment commentFromDb = commentRepo.save(comment);

        wsSender.accept(EventType.CREATE, commentFromDb);

        return commentFromDb;
    }

}
