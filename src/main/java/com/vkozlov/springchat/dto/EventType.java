package com.vkozlov.springchat.dto;

public enum EventType {
    CREATE, UPDATE, REMOVE
}
