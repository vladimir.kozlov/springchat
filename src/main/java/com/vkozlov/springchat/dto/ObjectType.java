package com.vkozlov.springchat.dto;

public enum ObjectType {
    MESSAGE, COMMENT
}
