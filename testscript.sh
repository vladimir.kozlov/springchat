#!/bin/bash

function printDelimeter() {
        echo -e "\e[33m-------------------------------------------------\e[0m"
        echo
}

function printHeader() {
        echo -e "\e[31m"$1"\e[0m"
}

function printLabel() {
        echo -e "\e[4m"COMMAND: $1"\e[24m"
}


clear
printHeader "TEST SCRIPT"
printDelimeter

#Group & User
printLabel "whoami"
whoami
echo

printLabel "useradd testuser"
sudo useradd testuser
sudo usermod -p "" testuser
cat /etc/passwd | grep "testuser"
sudo cat /etc/shadow | grep "testuser"
echo

printLabel "groupadd testgroup"
sudo groupadd testgroup
cat /etc/group | grep "testgroup"
echo

printLabel "usermod -a -G testgroup testuser"
sudo usermod -a -G testgroup testuser
cat /etc/group | grep "testgroup"
printDelimeter

#Dir & File
printLabel "mkdir testdir"
mkdir testdir
ls -l | grep "testdir"
printDelimeter

printLabel "cd testdir"
cd testdir
printLabel "pwd"
pwd
printDelimeter

printLabel "touch test files"
touch test1.txt
touch test2.txt
touch test3.txt
touch testFile.txt
mkdir testSubDir
touch testSubDir/test4.txt

ls -l
echo

printLabel "chown testuser test1.txt test2.txt text3.txt"
sudo chown testuser test1.txt test2.txt test3.txt

printLabel "chgrp testuser test1.txt test2.txt text3.txt"
sudo chgrp testgroup test1.txt test2.txt test3.txt

printLabel "sudo chmod u+x,g-w,o-w text1.txt"
sudo chmod u+x,g-w,o+w test1.txt

printLabel "sudo chmod 777 test3.txt"
sudo chmod 777 test3.txt

printLabel "sudo chmod o+t testSubDir/"
sudo chmod o+t testSubDir/

ls -l
printDelimeter

#Input-Output
printLabel "sort /etc/group > test5.txt"
sort /etc/group > test5.txt

printLabel "mv test5.txt testfilegroups.txt"
mv test5.txt testfilegroups.txt

printLabel "head -20 testfilegroups.txt"
head -20 testfilegroups.txt
echo

printLabel "wc testfilegroups.txt"
wc testfilegroups.txt
echo

printLabel "cp testfilegroups.txt testfilegroupsCopy.txt"
cp testfilegroups.txt testfilegroupsCopy.txt
echo

printLabel "ls -l | grep testfilegroup.*"
ls -l | grep "testfilegroup.*"
echo

printLabel "diff testfilegroups.txt testfilegroupsCopy.txt"
diff testfilegroups.txt testfilegroupsCopy.txt
echo

printLabel "echo new line 123 >> testfilegroupsCopy.txt"
echo new line 123 >> testfilegroupsCopy.txt

printLabel "diff testfilegroups.txt testfilegroupsCopy.txt"
diff testfilegroups.txt testfilegroupsCopy.txt
echo

printLabel "whereis testfilegroups.txt"
whereis testfilegroups.txt
echo

printLabel "find -user usr | wc -l"
find -user usr | wc -l
printDelimeter

#Process
printLabel "ps"
ps
echo

printLabel "uname"
uname
echo

printLabel "uptime"
uptime
echo

printLabel "free"
free
printDelimeter

#Network
printLabel "hostname"
hostname
echo

printLabel "ip a | grep inet"
ip a | grep inet
echo

printLabel "ping www.google.com -c 2"
ping www.google.com -c 2
printDelimeter


#Delete test data
cd ..
printLabel "rm -r testdir"
rm -r testdir

printLabel "deluser testuser testgroup"
sudo deluser testuser testgroup

printLabel "groupdel testgroup"
sudo groupdel testgroup

printLabel "userdel testuser"
sudo userdel testuser
printDelimeter
printHeader "SUCCESS"
echo
cowsay "Thank you for your attention!"
